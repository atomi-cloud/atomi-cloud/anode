package util

import (
	a "github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"testing"
)

type StreamCommandSuite struct {
	suite.Suite
}

func TestStreamCommandSuite(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}
	suite.Run(t, new(StreamCommandSuite))
}

func (suite *StreamCommandSuite) SetupTest() {
	CreateCommand("docker", "run", "--name=increment", "kirinnee/increment").
		Run()
}

func (suite *StreamCommandSuite) TearDownTest() {
	CreateCommand("docker", "container", "stop", "increment").
		Run()
	CreateCommand("docker", "rm", "increment").
		Run()
}

func (suite *StreamCommandSuite) TestBasicStream() {
	assert := a.New(suite.T())
	// set the test actual and expect
	actual := make([]string, 0, 20)
	expected := []string{"s0", "s1", "s2", "s3", "s4", "s5", "s6", "s7", "s8", "s9", "s10"}

	// stop signal
	sig := make(chan int)

	// Create the command object
	cmd := CreateCommand("docker", "logs", "increment", "-f")

	// Stream the output
	cmd.Stream(sig, func(output string) {
		actual = append(actual, "s"+output)
	})

	// Asserts
	assert.Equal(len(expected), len(actual))
	assert.EqualValues(expected, actual)
}
