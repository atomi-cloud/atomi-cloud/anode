package util

import (
	"strconv"
	"strings"
)

type StringBuilder struct {
	strings.Builder
}

func (b *StringBuilder) Write(r rune) {
	_, _ = b.WriteRune(r)
}
func (b *StringBuilder) To8() uint8 {
	a, _ := strconv.ParseUint(b.String(), 10, 8)
	return uint8(a)
}
func (b *StringBuilder) To16() uint16 {
	a, _ := strconv.ParseUint(b.String(), 10, 16)
	return uint16(a)
}
func (b *StringBuilder) To32() uint32 {
	a, _ := strconv.ParseUint(b.String(), 10, 32)
	return uint32(a)
}
