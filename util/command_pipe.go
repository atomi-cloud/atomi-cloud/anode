package util

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
)

type Command struct {
	command string
	arg     []string
}

type OutputEvent = func(string)

func CreateCommand(command string, arg ...string) Command {
	return Command{command, arg}
}

func (command Command) Run() {

	cmd := exec.Command(command.command, command.arg...)

	cmdReader, err := cmd.StderrPipe()
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, "error creating stdout pipe for cmd", err)
		return
	}

	// create scanner
	scanner := bufio.NewScanner(cmdReader)
	go func() {
		for scanner.Scan() {
			s := fmt.Sprint(scanner.Text())
			log.Println(s)
		}
	}()

	err = cmd.Start()
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, "error starting cmd", err)
		return
	}

	err = cmd.Wait()
	if err != nil {
		log.Println(err.Error())
		log.Fatalf("process finished with error = %v", err)
	}
	log.Print("process finished successfully")
}

func (command Command) Stream(sig chan int, onOut OutputEvent) {
	cmd := exec.Command(command.command, command.arg...)

	// create a pipe for the output of the script
	cmdReader, err := cmd.StdoutPipe()
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, "error creating stdout pipe for cmd", err)
		return
	}

	// create scanner
	scanner := bufio.NewScanner(cmdReader)
	go func() {
		for scanner.Scan() {
			s := fmt.Sprint(scanner.Text())
			onOut(s)
		}
	}()

	err = cmd.Start()
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, "error starting cmd", err)
		return
	}

	done := make(chan error, 1)
	go func() {
		done <- cmd.Wait()
	}()

	select {
	case <-sig:
		if err := cmd.Process.Kill(); err != nil {
			log.Fatal("failed to kill process: ", err)
		}
		log.Println("process killed by daemon")
	case err := <-done:
		if err != nil {
			log.Println(err.Error())
			log.Fatalf("process finished with error = %v", err)
		}
		//log.Print("process finished successfully")
	}
}
