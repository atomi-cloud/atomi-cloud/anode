package util

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"testing"
)

type StringBuilderSuite struct {
	suite.Suite
}

func TestStringBuilder(t *testing.T) {
	suite.Run(t, new(StringBuilderSuite))
}

func (s *StringBuilderSuite) TestWrite() {
	var v StringBuilder
	v.Write('a')
	v.Write('b')
	expected := "ab"
	actual := v.String()
	assert.Equal(s.T(), expected, actual)
}

func (s *StringBuilderSuite) TestTo8() {
	var v StringBuilder
	v.Write('1')
	v.Write('0')
	var expected uint8 = 10
	actual := v.To8()
	assert.Equal(s.T(), expected, actual)
}

func (s *StringBuilderSuite) TestTo16() {
	var v StringBuilder
	v.Write('1')
	v.Write('0')
	v.Write('0')
	var expected uint16 = 100
	actual := v.To16()
	assert.Equal(s.T(), expected, actual)
}

func (s *StringBuilderSuite) TestTo32() {
	var v StringBuilder
	v.Write('1')
	v.Write('2')
	v.Write('3')
	v.Write('4')
	v.Write('5')
	v.Write('6')
	v.Write('7')
	v.Write('8')
	v.Write('9')
	var expected uint32 = 123456789
	actual := v.To32()
	assert.Equal(s.T(), expected, actual)
}
