package metric_beat

type ContainerMetric struct {
	// Identifier
	ClusterName   string
	ServiceName   string
	ReplicaNumber uint8

	// CPU
	ProcessorPercentage string
	// Mem
	MemoryUsage string
	TotalMemory string
	// NetIO
	NetworkIn  string
	NetworkOut string
	// Disk IO
	DiskIn  string
	DiskOut string
}

type ServiceMetric struct {
	ClusterName string
	ServiceName string

	Containers []ContainerMetric
}

type Metric struct {
	MetricList map[string]*ServiceMetric
}

func (s *ServiceMetric) AddContainer(con ContainerMetric) {
	s.Containers = append(s.Containers, con)
}

func (metric *Metric) AddContainer(con ContainerMetric) {
	v, ok := metric.MetricList[con.ServiceName]
	if ok {
		v.AddContainer(con)
	} else {
		metric.MetricList[con.ServiceName] = &ServiceMetric{con.ClusterName, con.ServiceName, []ContainerMetric{con}}
	}
}
