package metric_beat

import (
	"gitlab.com/atomi-cloud/anode/util"
	"strings"
)

type MetricEvent func(metric Metric)

func StartMetric(sig chan int, event MetricEvent) {

	var metrics []ContainerMetric
	util.CreateCommand("docker", "stats", "--format", "{{.Name}} / {{.MemUsage}} / {{.CPUPerc}} / {{.NetIO}} / {{.BlockIO}}").
		Stream(sig, func(out string) {
			nextSession, normalize := IsNewSession(out)
			con := RawToContainerMetric(normalize)
			if nextSession {
				if metrics != nil && len(metrics) != 0 {
					metric := SortContainerMetricToServiceMetric(metrics)
					event(metric)
					metrics = nil
				}
			}
			metrics = append(metrics, con)
		})
}

func IsNewSession(raw string) (isNew bool, normalized string) {
	newSessionMarker := string([]byte{27, 91, 50, 74, 27, 91, 72})
	isNew = strings.HasPrefix(raw, newSessionMarker)
	if isNew {
		normalized = strings.TrimPrefix(raw, newSessionMarker)
	} else {
		normalized = raw
	}
	return
}

func IsMetricValid(raw string) bool {
	return strings.HasPrefix(raw, "--")
}

func nameDestructuring(name string) (clusterName string, serviceName string, replicaCount uint8) {
	var cluster, service, replica util.StringBuilder
	_cluster, _service, _replica := true, true, true
	for _, char := range name {
		if _cluster {
			if char == '_' {
				_cluster = false
			} else {
				cluster.Write(char)
			}
		} else if _service {
			if char == '.' {
				_service = false
			} else {
				service.Write(char)
			}
		} else if _replica {
			if char == '.' {
				_replica = false
			} else {
				replica.Write(char)
			}
		} else {
			break
		}
	}
	clusterName = cluster.String()
	serviceName = service.String()
	replicaCount = replica.To8()
	return
}

func RawToContainerMetric(raw string) ContainerMetric {
	slice := strings.Split(raw, " / ")
	cluster, service, replica := nameDestructuring(slice[0])
	return ContainerMetric{cluster, service, replica,
		slice[3], slice[1], slice[2],
		slice[4], slice[5], slice[6], slice[7]}
}

func SortContainerMetricToServiceMetric(containers []ContainerMetric) (x Metric) {
	m := make(map[string]*ServiceMetric)
	x = Metric{m}
	for _, con := range containers {
		x.AddContainer(con)
	}
	return
}
