package metric_beat

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"testing"
)

type MetricBeatsSuite struct {
	suite.Suite
}

func TestMetricBeats(t *testing.T) {
	suite.Run(t, new(MetricBeatsSuite))
}

func (s *MetricBeatsSuite) TestNameDestructuring() {
	testSubject := "app_app2.4.oewgsge2w7rg47is59c36l9mp "
	expectedCluster, expectedService, expectedReplica := "app", "app2", uint8(4)
	actualCluster, actualService, actualReplica := nameDestructuring(testSubject)
	assert.Equal(s.T(), expectedCluster, actualCluster)
	assert.Equal(s.T(), expectedService, actualService)
	assert.Equal(s.T(), expectedReplica, actualReplica)
}

func (s *MetricBeatsSuite) TestRawContainerMetric_should_convert_output_into_metric_struct() {
	testSubject := "app_app2.1.oewgsge2w7rg47is59c36l9mp / 5.055MiB / 1.934GiB / 0.03% / 718B / 0B / 2B / 3B"

	expected := ContainerMetric{"app", "app2", 1, "0.03%",
		"5.055MiB", "1.934GiB", "718B", "0B", "2B", "3B"}

	actual := RawToContainerMetric(testSubject)

	assert.Equal(s.T(), expected, actual)
}

func (s *MetricBeatsSuite) TestIsNewSession_should_be_true_if_starts_with_certain_bytes() {
	testSubject := string([]byte{27, 91, 50, 74, 27, 91, 72}) + "app_app2.1.oewgsge2w7rg47is59c36l9mp / 5.055MiB / 1.934GiB / 0.03% / 718B / 0B / 2B / 3B"
	actual, _ := IsNewSession(testSubject)
	assert.True(s.T(), actual)
}

func (s *MetricBeatsSuite) TestIsNewSession_should_return_normalize_log_if_it_is_new_session() {
	testSubject := string([]byte{27, 91, 50, 74, 27, 91, 72}) + "app_app2.1.oewgsge2w7rg47is59c36l9mp / 5.055MiB / 1.934GiB / 0.03% / 718B / 0B / 2B / 3B"
	expected := "app_app2.1.oewgsge2w7rg47is59c36l9mp / 5.055MiB / 1.934GiB / 0.03% / 718B / 0B / 2B / 3B"
	_, actual := IsNewSession(testSubject)
	assert.Equal(s.T(), expected, actual)
}

func (s *MetricBeatsSuite) TestIsNewSession_should_be_false_if_starts_with_normal_bytes() {
	testSubject := "app_app2.1.oewgsge2w7rg47is59c36l9mp / 5.055MiB / 1.934GiB / 0.03% / 718B / 0B / 2B / 3B"
	actual, _ := IsNewSession(testSubject)
	assert.False(s.T(), actual)

}

func (s *MetricBeatsSuite) TestNewSession_should_return_original_log_if_it_is_not_new_session() {
	testSubject := "app_app2.1.oewgsge2w7rg47is59c36l9mp / 5.055MiB / 1.934GiB / 0.03% / 718B / 0B / 2B / 3B"
	expected := "app_app2.1.oewgsge2w7rg47is59c36l9mp / 5.055MiB / 1.934GiB / 0.03% / 718B / 0B / 2B / 3B"
	_, actual := IsNewSession(testSubject)
	assert.Equal(s.T(), expected, actual)

}

func (s *MetricBeatsSuite) TestIsMetricValid_should_be_false_if_does_not_start_with_double_dash() {
	testSubject := "app_app2.1.oewgsge2w7rg47is59c36l9mp / 5.055MiB / 1.934GiB / 0.03% / 718B / 0B / 2B / 3B"
	assert.False(s.T(), IsMetricValid(testSubject))
}

func (s *MetricBeatsSuite) TestIsMetricValid_should_be_true_if_start_with_double_dash() {
	testSubject := "-- / 0B / 0B / 0.00% / 0B / 0B / 0B / 0B"
	assert.True(s.T(), IsMetricValid(testSubject))
}

func (s *MetricBeatsSuite) TestSortingContainerMetricCollectionToServiceMetric() {

	testSubj := []ContainerMetric{
		{"app", "app2", 1, "0.03%", "5.055MiB", "1.934GiB", "718B", "0B", "2B", "3B"},
		{"app", "app2", 2, "0.03%", "5.055MiB", "1.934GiB", "718B", "0B", "2B", "3B"},
		{"app", "app2", 3, "0.03%", "5.055MiB", "1.934GiB", "718B", "0B", "2B", "3B"},
		{"app", "app2", 4, "0.03%", "5.055MiB", "1.934GiB", "718B", "0B", "2B", "3B"},
		{"app", "app", 1, "0.03%", "5.055MiB", "1.934GiB", "718B", "0B", "2B", "3B"},
		{"app", "app", 2, "0.03%", "5.055MiB", "1.934GiB", "718B", "0B", "2B", "3B"},
		{"app", "app", 3, "0.03%", "5.055MiB", "1.934GiB", "718B", "0B", "2B", "3B"},
	}

	expected := Metric{
		map[string]*ServiceMetric{
			"app2": {"app", "app2", []ContainerMetric{
				{"app", "app2", 1, "0.03%", "5.055MiB", "1.934GiB", "718B", "0B", "2B", "3B"},
				{"app", "app2", 2, "0.03%", "5.055MiB", "1.934GiB", "718B", "0B", "2B", "3B"},
				{"app", "app2", 3, "0.03%", "5.055MiB", "1.934GiB", "718B", "0B", "2B", "3B"},
				{"app", "app2", 4, "0.03%", "5.055MiB", "1.934GiB", "718B", "0B", "2B", "3B"},
			}},
			"app": {"app", "app", []ContainerMetric{
				{"app", "app", 1, "0.03%", "5.055MiB", "1.934GiB", "718B", "0B", "2B", "3B"},
				{"app", "app", 2, "0.03%", "5.055MiB", "1.934GiB", "718B", "0B", "2B", "3B"},
				{"app", "app", 3, "0.03%", "5.055MiB", "1.934GiB", "718B", "0B", "2B", "3B"},
			}},
		},
	}

	actual := SortContainerMetricToServiceMetric(testSubj)
	assert.Equal(s.T(), expected, actual)

}
