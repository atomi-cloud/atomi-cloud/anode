package metric_beat

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/atomi-cloud/anode/util"
	"log"
	"testing"
	"time"
)

type MetricBeatsIntegrationSuite struct {
	suite.Suite
}

func TestMetricBeatsIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}
	suite.Run(t, new(MetricBeatsIntegrationSuite))
}

func (s *MetricBeatsIntegrationSuite) SetupSuite() {
	log.Println("Start setup")
	util.CreateCommand("docker", "stack", "deploy", "--compose-file", "../integration2.yml", "--prune", "--resolve-image", "changed", "metric").Run()
	log.Println("Complete setup")
}

func (s *MetricBeatsIntegrationSuite) TearDownSuite() {
	log.Println("Start teardown")
	util.CreateCommand("docker", "stack", "rm", "metric").Run()
	log.Println("complete teardown")

}

func (s *MetricBeatsIntegrationSuite) TestLoggingDockerStatsMetric() {
	// set the test actual and expect
	actual := make([]Metric, 0, 30)

	time.Sleep(10 * time.Second)
	// stop signal
	sig := make(chan int)
	go func() {
		// Stream the output
		StartMetric(sig, func(m Metric) {
			actual = append(actual, m)
		})
	}()

	time.Sleep(5 * time.Second)
	sig <- 0
	// Asserts
	for _, metric := range actual {
		assert.Contains(s.T(), metric.MetricList, "app")
		assert.Len(s.T(), metric.MetricList["app"].Containers, 4)
	}
}
