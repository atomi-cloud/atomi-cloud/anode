package metric_beat

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"testing"
)

type MetricSuite struct {
	suite.Suite
}

func TestMetric(t *testing.T) {
	suite.Run(t, new(MetricSuite))
}

func createContainerMetric(cluster, service string, replica uint8) ContainerMetric {
	return ContainerMetric{cluster, service, replica, "0.03%", "5.055MiB", "1.934GiB", "718B", "0B", "2B", "3B"}
}

func (s *MetricSuite) TestServiceMetricAddContainer() {
	con1 := createContainerMetric("app", "app2", 1)
	con2 := createContainerMetric("app", "app2", 2)

	testSubj := ServiceMetric{"app", "app2", []ContainerMetric{con1}}
	expected := ServiceMetric{"app", "app2", []ContainerMetric{con1, con2}}

	testSubj.AddContainer(con2)
	assert.Equal(s.T(), expected, testSubj)
}

func (s *MetricSuite) TestMetricAddContainerToExistingServiceMetric() {

	con1 := createContainerMetric("app", "app", 1)
	con2 := createContainerMetric("app", "app", 2)

	ser1 := ServiceMetric{"app", "app", []ContainerMetric{con1}}

	expectedService := ServiceMetric{"app", "app", []ContainerMetric{con1, con2}}
	expectedMap := make(map[string]*ServiceMetric)
	expectedMap["app"] = &expectedService
	expectedMetric := Metric{expectedMap}

	m := make(map[string]*ServiceMetric)
	m["app"] = &ser1
	metric := Metric{m}
	metric.AddContainer(con2)

	assert.Equal(s.T(), expectedMetric, metric)

}

func (s *MetricSuite) TestMetricAddContainerToNewServiceMetric() {
	con1 := createContainerMetric("app", "app", 1)
	con2 := createContainerMetric("app", "app2", 1)

	actualCon := createContainerMetric("app", "app2", 1)

	ser1 := ServiceMetric{"app", "app", []ContainerMetric{con1}}
	ser2 := ServiceMetric{"app", "app2", []ContainerMetric{con2}}

	expectedMap := make(map[string]*ServiceMetric)
	expectedMap["app"] = &ser1
	expectedMap["app2"] = &ser2
	expectedMetric := Metric{expectedMap}

	m := make(map[string]*ServiceMetric)
	m["app"] = &ser1
	metric := Metric{m}
	metric.AddContainer(actualCon)

	assert.Equal(s.T(), expectedMetric, metric)
}
