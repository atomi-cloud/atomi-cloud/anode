package main

import (
	"encoding/json"
	"flag"
	"gitlab.com/atomi-cloud/anode/logs_beat"
	"gitlab.com/atomi-cloud/anode/metric_beat"
	"log"
	"net/http"
)

var addr = flag.String("addr", ":8080", "http service address")

func main() {

	flag.Parse()
	metricHub := newHub()
	go metricHub.run()

	logsHub := newHub()
	go logsHub.run()

	go func() {
		metricLoggingChannel := make(chan int)
		metric_beat.StartMetric(metricLoggingChannel, func(out metric_beat.Metric) {
			b, err := json.Marshal(out)
			if err != nil {
				log.Println()
			}
			metricHub.broadcast <- b
		})
	}()

	log.Println("metic started")

	go func() {
		Stream(func(a []string) {
			Update(a, func(s string) chan int {
				logLoggingChannel := make(chan int)
				go func(e string) {
					logs_beat.StartLog(logLoggingChannel, e, func(out logs_beat.Log) {
						b, err := json.Marshal(out)
						if err != nil {
							log.Println()
						}
						logsHub.broadcast <- b
					})
				}(s)
				return logLoggingChannel
			})
		})
	}()
	log.Println("logging started")

	http.HandleFunc("/metric", func(w http.ResponseWriter, r *http.Request) {
		serveWs(metricHub, w, r)
	})

	http.HandleFunc("/logs", func(w http.ResponseWriter, r *http.Request) {
		serveWs(logsHub, w, r)
	})
	log.Println("handled funcs")
	err := http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}

}
