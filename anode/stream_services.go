package main
import (
	"gitlab.com/atomi-cloud/anode/util"
	"time"
)

func Stream(s func([]string)) {
	c := make(chan int)
	for  {
		services := make([]string, 0)
		util.CreateCommand("docker", "service", "ls", "--format" ,"{{.Name}}").Stream(c, func(log string) {
			services = append(services, log)
		})
		s(services)
		time.Sleep(1000)
	}

}