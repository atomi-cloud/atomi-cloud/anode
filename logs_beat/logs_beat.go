package logs_beat

import "gitlab.com/atomi-cloud/anode/util"

type TimeStamp struct {
	Year       uint16
	Month      uint8
	Day        uint8
	Hour       uint8
	Min        uint8
	Second     uint8
	NanoSecond uint32
	Timezone   uint8
}

type Log struct {
	TimeStamp   string
	ClusterName string
	ServiceName string
	Replica     uint8
	Id          string
	Log         string
}

type LogEvent = func(Log)

func StartLog(sig chan int, serviceName string, onLog LogEvent) {
	util.CreateCommand("docker", "service", "logs", "-f", "-t", serviceName).Stream(sig, func(log string) {
		logInstance := RawLogToStruct(log)
		onLog(logInstance)
	})
}

func RawLogToStruct(log string) Log {
	//var year, month, day, hour, min, second, nanoSecond, timezone util.StringBuilder
	var timeStamp util.StringBuilder
	readCluster, readService, readReplica, readId, flush := true, true, true, true, true
	var cluster, service, replica, id, line util.StringBuilder
	for pos, char := range log {
		if pos < 28 {
			timeStamp.Write(char)
		} else if readCluster {
			if char == '_' {
				readCluster = false
			} else {
				if pos > 30 {
					cluster.Write(char)
				}
			}
		} else if readService {
			if char == '.' {
				readService = false
			} else {
				service.Write(char)
			}
		} else if readReplica {
			if char == '.' {
				readReplica = false
			} else {
				replica.Write(char)
			}
		} else if readId {
			if char == '@' {
				readId = false
			} else {
				id.Write(char)
			}
		} else if flush {
			if char == '|' {
				flush = false
			}
		} else {
			line.Write(char)
		}
	}
	//stamp := TimeStamp{
	//	year.To16(),
	//	month.To8(),
	//	day.To8(),
	//	hour.To8(),
	//	min.To8(),
	//	second.To8(),
	//	nanoSecond.To32(),
	//	timezone.To8(),
	//}
	return Log{
		timeStamp.String(),
		cluster.String(),
		service.String(),
		replica.To8(),
		id.String(),
		line.String()[1:],
	}
}
