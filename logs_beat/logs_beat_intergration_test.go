package logs_beat

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/atomi-cloud/anode/util"
	"log"
	"testing"
	"time"
)

type LogsBeatsIntegrationSuite struct {
	suite.Suite
}

func TestLogsBeatsIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}
	suite.Run(t, new(LogsBeatsIntegrationSuite))
}

func (s *LogsBeatsIntegrationSuite) SetupSuite() {
	log.Println("Start setup")
	util.CreateCommand("docker", "stack", "deploy", "--compose-file", "../integration.yml", "--prune", "--resolve-image", "changed", "beats").Run()
	log.Println("Complete setup")
}

func (s *LogsBeatsIntegrationSuite) TearDownSuite() {
	log.Println("Start teardown")
	util.CreateCommand("docker", "stack", "rm", "beats").Run()
	log.Println("complete teardown")

}

func (s *LogsBeatsIntegrationSuite) TestLoggingDockerStackLog() {
	// set the test actual and expect
	actual := make([]string, 0, 10)
	expected := []string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}

	// stop signal
	sig := make(chan int)
	go func() {
		// Stream the output
		StartLog(sig, "beats_app", func(log Log) {
			actual = append(actual, log.Log)
		})
	}()

	time.Sleep(5 * time.Second)
	sig <- 0
	// Asserts
	assert.Equal(s.T(), len(expected), len(actual))
	assert.EqualValues(s.T(), expected, actual)
}

func (s *LogsBeatsIntegrationSuite) TestLoggingDockerStackReplica() {
	// set the test actual and expect
	actual := make([]uint8, 0, 10)
	expected := []uint8{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}

	// stop signal
	sig := make(chan int)
	go func() {
		// Stream the output
		StartLog(sig, "beats_app", func(log Log) {
			actual = append(actual, log.Replica)
		})
	}()

	time.Sleep(5 * time.Second)
	sig <- 0
	// Asserts
	assert.Equal(s.T(), len(expected), len(actual))
	assert.EqualValues(s.T(), expected, actual)
}

func (s *LogsBeatsIntegrationSuite) TestLoggingDockerStackClusterName() {
	// set the test actual and expect
	actual := make([]string, 0, 11)
	expected := []string{"beats", "beats", "beats", "beats", "beats", "beats", "beats", "beats", "beats", "beats", "beats"}

	// stop signal
	sig := make(chan int)
	go func() {
		// Stream the output
		StartLog(sig, "beats_app", func(log Log) {
			actual = append(actual, log.ClusterName)
		})
	}()

	time.Sleep(10 * time.Second)
	sig <- 0
	// Asserts
	assert.Equal(s.T(), len(expected), len(actual))
	assert.EqualValues(s.T(), expected, actual)
}

func (s *LogsBeatsIntegrationSuite) TestLoggingDockerStackServiceName() {
	// set the test actual and expect
	actual := make([]string, 0, 11)
	expected := []string{"app", "app", "app", "app", "app", "app", "app", "app", "app", "app", "app"}

	// stop signal
	sig := make(chan int)
	go func() {
		// Stream the output
		StartLog(sig, "beats_app", func(log Log) {
			actual = append(actual, log.ServiceName)
		})
	}()

	time.Sleep(5 * time.Second)
	sig <- 0
	// Asserts
	assert.Equal(s.T(), len(expected), len(actual))
	assert.EqualValues(s.T(), expected, actual)
}
