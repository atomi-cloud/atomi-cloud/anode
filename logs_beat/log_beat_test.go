package logs_beat

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"testing"
)

type LogBeatsSuite struct {
	suite.Suite
}

func TestLogBeats(t *testing.T) {
	suite.Run(t, new(LogBeatsSuite))
}

func (s *LogBeatsSuite) TestRawLogToInstance() {
	testSubj := "2019-06-20T05:05:57.297752200Z app_app2.1.3mr5404ykn3i@linuxkit-00155d00cc01    | a1 75"
	expectedTimeStamp := TimeStamp{2019, 6, 20, 5, 5, 57, 2977522, 0}
	expected := Log{expectedTimeStamp, "app", "app2", 1, "3mr5404ykn3i", "a1 75"}

	// Test
	actual := RawLogToStruct(testSubj)
	assert.Equal(s.T(), expected, actual)
}
